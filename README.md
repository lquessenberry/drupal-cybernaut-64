# Drupal 9 and Twig Pattern Lab

A working example of Drupal 9 being used with Pattern Lab (Twig) to drive Atomic Design in custom theme development.

# Introduction

# Pattern Lab & Drupal 9
## What, When, Where, Why, and How

---

class: center, middle

# What
### What is Atomic Design?  
https://atomicdesign.bradfrost.com/chapter-2/  
  
### What is Pattern Lab?  
https://atomicdesign.bradfrost.com/chapter-3/  
https://patternlab.io/  


---

class: center, middle

# When
### When to use Pattern Lab?  

https://www.pivale.co/blog/what-is-pattern-lab-and-why-should-i-care-about-it  

---

class: center, middle

# Where
### Where to install Pattern Lab?  
  
_Short Answer:_ On your local machine.  
  
_Long Answer:_ Install on a common repository, a staging environment, or any place where your front-end team gains from having access to it. In our case, we'll install inside of our custom Drupal theme and make components from Pattern Lab directly useable within Drupal. This allows designers, front-end devs, and back-end devs alike to work effectively together with an authorative source for theme design.

---

class: center, middle

# Why
Why Pattern Lab?
Why not use XYZ instead?

---

class: center, middle

# How
How do you install and integrate with Drupal?
How well does it work?

---

class: center, middle

# Resources

https://www.pivale.co/blog/what-is-pattern-lab-and-why-should-i-care-about-it  
https://patternlab.io/  
https://gitlab.com/lquessenberry/d9pl  
https://atomicdesign.bradfrost.com/table-of-contents/  


---

class: center, middle

# Thanks and Stay In Touch
### Lee Quessenberry
https://linkedin.com/in/lquessenberry  
https://esteemed.slack.com
